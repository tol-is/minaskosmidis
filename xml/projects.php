
<?php
    

    header ("Content-Type:text/html");
    $xml = simplexml_load_file("xml.xml");


    $posts = $xml->xpath("DB/PROJECT");

    $count = count($posts);


    foreach($posts as $post)
    { 
       // print_r($post);
        
        $slug = $post["link"];

        $dirName = 'projects/'.$count."_".$slug;
        
        if(!is_dir($dirName))
        {
            mkdir($dirName, 0777);
            
        }

        copy('desc.txt', $dirName.'/desc.txt');
            

        $data=$post->{"EN"};
        $title=$data->{"TITLE"};
        $subtitle=$data->{"TITLE2"};
        $date=$data->{"INF"};
        
        file_put_contents($dirName.'/desc.txt', $title."\n".$subtitle."\n".$date);
        
        $count--;
        
        $images = $post->xpath("ITEM");
		
		$imgcount = 1;
        foreach ($images as $img) {
            
			if($img["thumb"] && $imgcount==1)
			{
				copy('uploads/'.$img["thumb"], $dirName.'/thumb_'.$slug.'.jpg');
			}
            copy('uploads/'.$img["large"], $dirName.'/img_'.$imgcount.'_'.$slug.'.jpg');
			$imgcount++;
        }

    }




?>