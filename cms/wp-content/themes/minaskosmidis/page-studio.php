<?php
/*
Template Name: Studio Page
*/
?>

<?php get_header(); ?>
			


<?php
$data = array(
		'id' => 'studio',
		'grid' => array(
		)
	);
?>

<?php if (have_posts()) : ?>

<?php while (have_posts()) : the_post(); ?>
	
	<?php 
		include("pages/studioJson.php");
	?>
				
	<?php endwhile; ?>	
					
	<?php else : ?>
			
		Not found		
 			
<?php endif; ?>

<?php
	echo json_encode($data);
?>
	
			
<?php get_footer(); ?>