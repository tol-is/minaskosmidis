<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images, 
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
    - head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
    - custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once('library/bones.php'); // if you remove this, bones will break


/*
2. Custom Post types
*/
require_once('library/custom-post-types.php'); 


/*
3. library/admin.php
    - removing some default WordPress dashboard widgets
    - an example custom dashboard widget
    - adding custom login css
    - changing text in footer of admin
*/

require_once('library/admin.php'); // this comes turned off by default
/*
	
	

/************* IMAGE SIZE OPTIONS *************/
	add_image_size( "preview", 100, 9999, false );

//If Mobile use medium

//else
 //grid 1 to 6 use medium
 //grid 6 ro 12 use large


?>