<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/


// let's create the function for the custom type
function custom_post_types() { 


	register_post_type( 'project', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Projects', 'minaskosmidis'), /* This is the Title of the Group */
			'singular_name' => __('Project', 'minaskosmidis'), /* This is the individual type */
			'all_items' => __('Projects', 'minaskosmidis'), /* the all items menu item */
			'add_new' => __('Add New', 'minaskosmidis'), /* The add new menu item */
			'add_new_item' => __('Add New Project', 'minaskosmidis'), /* Add New Display Title */
			'edit' => __( 'Edit', 'minaskosmidis' ), /* Edit Dialog */
			'edit_item' => __('Edit Project', 'minaskosmidis'), /* Edit Display Title */
			'new_item' => __('New Project', 'minaskosmidis'), /* New Display Title */
			'view_item' => __('View Project', 'minaskosmidis'), /* View Display Title */
			'search_items' => __('Search Projects', 'minaskosmidis'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'minaskosmidis'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'minaskosmidis'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the project post type', 'minaskosmidis' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'project', 'with_front' => false ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title')
			//'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */


	register_post_type( 'news', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('News', 'minaskosmidis'), /* This is the Title of the Group */
			'singular_name' => __('News', 'minaskosmidis'), /* This is the individual type */
			'all_items' => __('All News', 'minaskosmidis'), /* the all items menu item */
			'add_new' => __('Add New', 'minaskosmidis'), /* The add new menu item */
			'add_new_item' => __('Add New', 'minaskosmidis'), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __('Edit News', 'bonestheme'), /* Edit Display Title */
			'new_item' => __('New', 'bonestheme'), /* New Display Title */
			'view_item' => __('View News', 'bonestheme'), /* View Display Title */
			'search_items' => __('Search News', 'bonestheme'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'bonestheme'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'bonestheme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the news post type', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'news', 'with_front' => false ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title')
	 	) /* end of options */
	); /* end of register post type */

	register_post_type( 'publication', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Publications', 'minaskosmidis'), /* This is the Title of the Group */
			'singular_name' => __('Publications', 'minaskosmidis'), /* This is the individual type */
			'all_items' => __('All Publications', 'minaskosmidis'), /* the all items menu item */
			'add_new' => __('Add Publication', 'minaskosmidis'), /* The add new menu item */
			'add_new_item' => __('Add New Publication', 'minaskosmidis'), /* Add New Display Title */
			'edit' => __( 'Edit Publication', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __('Edit Publications', 'bonestheme'), /* Edit Display Title */
			'new_item' => __('New Publication', 'bonestheme'), /* New Display Title */
			'view_item' => __('View Publications', 'bonestheme'), /* View Display Title */
			'search_items' => __('Search Publications', 'bonestheme'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'bonestheme'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'bonestheme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the news post type', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'pubs', 'with_front' => false ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title')
	 	) /* end of options */
	); /* end of register post type */


	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_types');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
    register_taxonomy( 'project_cat', 
    	array('project'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Project Categories', 'minaskosmidis' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Project Category', 'minaskosmidis' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Project Categories', 'minaskosmidis' ), /* search title for taxomony */
    			'all_items' => __( 'All Project Categories', 'minaskosmidis' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Project Category', 'minaskosmidis' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Project Category:', 'minaskosmidis' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Project Category', 'minaskosmidis' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Project Category', 'minaskosmidis' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Project Category', 'minaskosmidis' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Project Category Name', 'minaskosmidis' ) /* name title for taxonomy */
    		),
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'projects' ),
    	)
    );   
    
    
    /*
    	looking for custom meta boxes?
    	check out this fantastic tool:
    	https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
    */
	

?>