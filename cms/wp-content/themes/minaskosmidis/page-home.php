<?php
/*
Template Name: Home Page
*/
?>

<?php
global $paged;
global $query_string;
?>

<?php get_header(); ?>

<?php 
	$paged = (get_query_var('page')) ? get_query_var('page') : 1;
	$do_not_show_stickies = 1; // 0 to show stickies
	$args=array(
		'post_type' => array('news'),
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'paged' => $paged,
		'posts_per_page' => $post_per_page,
		'caller_get_posts' => $do_not_show_stickies
	);
	$wp_query = new WP_Query($args);
	$maxnumpages = $wp_query->max_num_pages;
?>

<?php
$data = array(
		'id' => 'news',
		'page' => $paged,
		'maxpages' => $maxnumpages,
		'rows' => array(
		)
	);
?>

<?php if (have_posts()) : ?>

<?php while (have_posts()) : the_post(); ?>
	
	<?php 
		include("pages/newsLayout.php");
	?>
				
	<?php endwhile; ?>	
					
	<?php else : ?>
			
		Not found		
 			
<?php endif; ?>

<?php
	echo json_encode($data);
return;
?>
	






<?php

echo $paged;
echo " | ";
echo $maxnumpages;
echo "<br>";
?>


<?php if ($maxnumpages > 1 ) : ?>
				<div id="nav-below">
				<div class="nav-float">
					<?php previous_posts_link( __( '<span class="nav-previous">Previous</span>', 'landfilleditions' ) ); ?>
					<?php if($paged>1) echo " | "; ?>
					<?php echo "Page ".$paged." of ".$maxnumpages; ?>
					<?php if($paged < $maxnumpages ) echo " | "; ?>
					<?php next_posts_link( __( '<span class="nav-next">Next</span>', 'landfilleditions' ) ); ?>
				</div>	
				</div><!-- #nav-below -->
<?php endif; ?>

<?php get_footer(); ?>