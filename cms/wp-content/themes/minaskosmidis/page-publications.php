<?php
/*
Template Name: Publications Page
*/
?>


<?php get_header(); ?>

<?php 
	$do_not_show_stickies = 1; // 0 to show stickies
	$args=array(
		'post_type' => array('publication'),
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'caller_get_posts' => $do_not_show_stickies
	);
	$wp_query = new WP_Query($args);
	$maxnumpages = $wp_query->max_num_pages;
?>
	

<?php
$data = array(
		'id' => 'publications',
		'publications' => array(
		)
	);
?>

<?php if (have_posts()) : ?>

<?php while (have_posts()) : the_post(); ?>
	
		<?php 
			include("pages/publicationsJson.php");
		?>
				
	<?php endwhile; ?>	
					
	<?php else : ?>
			
		Nothing found		
 			
<?php endif; ?>
<?php
	echo json_encode($data);
?>

<?php get_footer(); ?>