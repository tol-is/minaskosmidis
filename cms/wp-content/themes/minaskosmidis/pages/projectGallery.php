<?php

	$gallery = get_field('project_gallery');
	
	$gal=array();
	//print_r($gallery);
	foreach ($gallery as $image) {
			
		$sizes = $image['sizes'];

		$imagerow = array(
			'caption' => $image['caption'],
			'sizes' => array()
		);
		
		foreach($sizes as $size){
			
			$key = key($sizes);

			if($key=="preview" || $key=="thumbnail" )
			{

			}
			else
			{
				$img = wp_get_attachment_image_src( $image['id'],$key);
				$imagerow['sizes'][$key] = $img;
			}

			next($sizes); 
		}

		$img = wp_get_attachment_image_src( $image['id'],"full");
		$imagerow['sizes']["full"] = $img;

		$gal[] = $imagerow;
	}

	$row['gallery'] = $gal;
?>