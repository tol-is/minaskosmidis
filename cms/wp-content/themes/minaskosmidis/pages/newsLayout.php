<?php

	
	$news_link_to_content = get_field("news_link_to_content");
	$news_link_to_outside_world = get_field("news_link_to_outside_world");

	$type = $news_link_to_content->post_type;
	$slug = $news_link_to_content->post_name;
	
	$permalink ="";
	if($type!="page")
	{
		$permalink.=$type."/";
	}
	$permalink.=$slug;


	$row = array();

	$row['id'] = $post->ID;
	$row['title'] = $post->post_title;
	$row['permalink'] = $permalink;
	$row['external_link'] = $news_link_to_outside_world;
	$row['content'] = array();

	while(the_flexible_field("news_Content")):

		$rowContent = array();
		
		$rowType = get_row_layout();
		
		$rowContent['type'] = $rowType;
		
		switch ($rowType) {
			case 'news_image_box':
			
				$rowContent['image_object'] = get_sub_field("news_entry_image_file");
			
				$sizes = $rowContent['image_object']['sizes'];
				
				foreach($sizes as $size){
					
					$key = key($sizes);
	
					$img = wp_get_attachment_image_src( $rowContent['image_object']['id'],$key);
					
				   	$rowContent['image_object']['sizes'][$key] = $img;
				    next($sizes); 
				}

				$rowContent['image_width'] = get_sub_field("news_entry_image_width");
				break;
			
			case 'news_push_box':
				$rowContent['push_width'] = get_sub_field("news_push_width");
				break;

			case 'news_caption_box':
				$rowContent['caption_copy'] = get_sub_field("news_caption");
				$rowContent['caption_width'] = get_sub_field("news_caption_width");
				$rowContent['caption_pos'] = get_sub_field("news_caption_position");
				break;

			case 'news_quote_box':
				$rowContent['quote_copy'] = get_sub_field("news_quote");
				$rowContent['quote_width'] = get_sub_field("news_quote_width");
				$rowContent['quote_size'] = get_sub_field("news_quote_size");
				break;
		}


		$row['content'][] = $rowContent;

	endwhile;

	$data['rows'][] = $row;

?>