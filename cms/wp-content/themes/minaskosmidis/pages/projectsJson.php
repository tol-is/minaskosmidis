<?php

	$row = array();

	//$post_categories = get_categories('taxonomy=project_cat&type=project'); 


	//$post_categories = get_the_term_list( $post->ID, 'project_cat'); 
	///wp_get_post_categories( $post->ID );
	$wpterms = get_the_terms( $post->ID , 'project_cat' );
	
	$terms = array();

	if($wpterms)
	{
		foreach( $wpterms as $wpterm )
		{
			$termobj = array();
			$termobj["id"] = $wpterm->term_id;
			$termobj["slug"] = $wpterm->slug;
			$terms[] = $termobj;
		}
	}




	$row['id'] = $post->ID;
	$row['permalink'] = $post->post_name;
	$row['categories'] = $terms;
	$row['project_title'] = get_field('project_title');
	$row['project_location'] = get_field('project_location');
	$row['project_date'] = get_field('project_date');

	$thumb = get_field('project_thumb');

	$row['thumb'] = wp_get_attachment_image_src( $thumb['id'],"medium");
	$row['thumb']['grid_size'] = get_field('thumb_grid_size');
	
	$row['gallery'] = array();
	//include("projectGallery.php");

	$data['projects'][] = $row;	
?>
