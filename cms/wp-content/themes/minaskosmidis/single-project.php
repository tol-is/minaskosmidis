<?php
/*
This is the custom post type post template.
If you edit the post type name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom post type is called
register_post_type( 'bookmarks',
then your single template should be
single-bookmarks.php

*/
?>

<?php get_header(); 

	$row['id'] = $post->ID;
	$row['permalink'] = $post->post_name;
	$row['project_title'] = get_field('project_title');
	$row['project_location'] = get_field('project_location');
	$row['project_date'] = get_field('project_date');
	$row['project_info'] = get_field('project_info');

	$thumb = get_field('project_thumb');

	$row['thumb'] = wp_get_attachment_image_src( $thumb['id'],"medium");
	
	

	$row['gallery'] = array();

	include("pages/projectGallery.php");

	echo json_encode($row);

 get_footer(); ?>