<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'minaskosmidis_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%viX618k|Cqwc,~+[+gqbB^%h2ma.,NFbn,w`sSJ/0kkatV<tYWK6VFRjUM>)2bV');
define('SECURE_AUTH_KEY',  '<#z-JQ=yX&s5cw2eGf:Av*V+Ku4co}w?>>^Mv/vyF]I^7B<w7{^s*#}+u?|jV,K`');
define('LOGGED_IN_KEY',    '5@fjCguiYwu0>~d|S*PId|YH>$<v8t+Har8=34HsmGMXLk%Q@@Eie},4BJ@~U_<]');
define('NONCE_KEY',        'qf?s&2]LsmRzDyG9Pz6^V81f3cs_^zsndU%1C~9j|t3!F4)o[5)JUO(5bMPC~N8n');
define('AUTH_SALT',        'P:ay=cw:`e^?I):+iVWT#H=n^B~JTesk#0 %EP4le9j1h4>#@$y%Pjtw*(50{aNr');
define('SECURE_AUTH_SALT', 'sk~G@91`e@*CJ#L^WH(42)UHPX8eZJJ)`VB;`$wMY|lxlU!YF2x^nTl9)O*EOQ)@');
define('LOGGED_IN_SALT',   'G4WYBd04MdH`2p<hLoHijpgIr9LS+M+b ,r=_^9}a;>.PS[1@j2~:#!>s^M;JE];');
define('NONCE_SALT',       'O@|<`<@xOJwM8wx 6K:4KDQ6|>#@xU@m(-DmN;kWRBkvuF&JyIv-Sp^Q?davlFlD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
