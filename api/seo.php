<?php

    $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $output = rtrim($url, '/');
    $path = explode('/', $output);
    
    $section = $path[count($path) - 1];
    $projectid = "";

    if(count($path) === 5 && $path[3] === "project") {
        $section = $path[count($path) - 2];
        $projectid = $path[count($path) - 1];
    }


    $title = '';
    $description = '';
    $keywords = "Minas Kosmidis, Architecture, Interior Design, Thessaloniki, Greece, ";
    $imageSrc= '';
    $seoMarkup = '';

    if($section == 'news' || $section == 'minaskosmidis.com' || $section == 'news')
    {
       
        $title = 'News - Minas Kosmidis - Architecture, Interior Design';
        $description = 'Minas Kosmidis - Architecture, Interior Design';
        $imageSrc = "http://minaskosmidis.com/images/screen.jpg";

    } 
    else if($section == 'projects')
    {
        
        $title = 'Projects - Minas Kosmidis - Architecture, Interior Design';
        $description = 'Minas Kosmidis - Architecture, Interior Design';
        $imageSrc = "http://minaskosmidis.com/images/screen.jpg";
    } 

    else if($section == 'publications') 
    {
        
        $title = 'Publications - Minas Kosmidis - Architecture, Interior Design';
        $description = 'Minas Kosmidis - Architecture, Interior Design';
        $imageSrc = "http://minaskosmidis.com/images/screen.jpg";

    } 
    else if($section == 'studio')
    {
        
        $title = 'Studio - Minas Kosmidis - Architecture, Interior Design';
        $description = 'Every project is a soul’s deposit, aiming at a proposal that is functionally integral and aesthetically unique, where the director shapes figures, entering and not invading to them. Through an architectural approach, he creates emotional environments, filtering the connection between needed and wanted, as well as between function and the characters’ aesthetics.';
        $imageSrc = "http://minaskosmidis.com/images/studio.jpg";

        $seoMarkup = "<p>Filellinon 55, Panorama, 552 36, Thessaloniki, Greece, t. +30 2310 344651, f. +30 2310 344656, info@minaskosmidis.com<br></p>";
        $seoMarkup .= "<img src=\"http://minaskosmidis.com/cms/wp-content/uploads/2012/09/DSC3275ok-768x419.jpg\" >";
        $seoMarkup .= "<p>«… an architect is called to enact the role of a director in a play where the script is written by the heroes-clients, and which takes place in a particular space and time scene».</p>";
        $seoMarkup .= "<p>Every project is a soul’s deposit, aiming at a proposal that is functionally integral and aesthetically unique, where the director shapes figures, entering and not invading to them. Through an architectural approach, he creates emotional environments, filtering the connection between needed and wanted, as well as between function and the characters’ aesthetics.</p>";
        $seoMarkup .= "<img src=\"http://minaskosmidis.com/cms/wp-content/uploads/2012/09/minas_kosmidis-768x1147.jpeg\" >";
        $seoMarkup .="<p><b>Minas Kosmidis - Architect</b><br>Studio Minas Kosmidis [Architecture In Concept] is the evolution of the architect’s private studio and its relocation, in 2007, from Komotini to the city of Thessaloniki.<br>Architect Minas Kosmidis graduated from the School of Architecture of the Aristotle University of Thessaloniki in 1988 and in 1991 he completed his postgraduate studies in \"Industrial Design\", with a scholarship from \"EOMMEX\", at the École d’ Architecture de Paris-Conflans in the department of \"Étude et Creation de Mobilier\". During his Master studies he worked together with architect & designer Galeriste Lamia Hassanaein, in Cairo and Paris.<br>In 1993 he establishes his architectural studio in the town of Komotini, with operations around Greece. The studio undertakes private projects, both housing and professional, with a specialization in the area of dining and entertainment establishments.<br>Using the abstraction, the neatness of lines, the clarity, the transparency, the symmetry, the flow, the balance of volumes, the elements of nature and the light as tools, he is inspired to create unique projects, which combine full functionality and unique aesthetics.<br>The studio’s projects have been published in many architecture, interior-design magazines and websites in Greece and abroad.</p>";
        $seoMarkup .="<p><b>Client List</b><br>Greek Textile Industry, Thracian Ginning Factories, Hotel Filia I.C., Hotel Alexandra Beach I.C., Zigosis L.R.C., Xrisi Xina L.R.C., Kalligeuston L.R.C., V. Hatzivasileiou- Akka, J. Hatzhnikolaou, Stefanou I.C, G. Poppi, Ι.Poppi, K. Konstantinidis, P. & E. Papazoglou, Ga Plastic Surgery-A. Gianopoulou, Area Domus-Kartell - X. Xristodoulou, G LIST-E. Karananou, Poiotiki Estiasi I.C.- I. Pappas, Hyperco – G. Alexiadis, N. Hatzitheodoroy, etc.</p>";
    }

    



    include('classes/db.php');
    include('classes/api.php');
    include('classes/project.php');
    
    $api = new API();

    if($section == 'project'){


        $post = $api->getProject($projectid);
 
            $data = array();

            $data['id'] = $post->id;
            $data['title'] = $post->title;
            $data['link'] = $post->link;
            $data['location'] = $post->location;
            $data['date'] = $post->date;
            $data['thumbnail'] = $post->thumbnail;
            $data["gallery"] = $post->gallery;

            $title = $post->title." by Minas Kosmidis";
            $description = $post->title." - ".$post->location.", ".$post->date." - A project by Minas Kosmidis - Architecture, Interior Design ";
            $imageSrc = "http://minaskosmidis.com/cms/wp-content/uploads/".$post->thumbnail;
            $keywords.= $post->title.", ".$post->location.", ".$post->date;
            $seoMarkup = "<h1>".$post->title." by Minas Kosmidis</h1>";
            $seoMarkup.= "<p>".$post->location.", ".$post->date."</p>";
            foreach ($post->gallery as $img) {
                $seoMarkup.= "<img src=\"http://minaskosmidis.com/cms/wp-content/uploads/".$img."\" >";
            }
    }



    if($section == 'projects'){

        $api = new API();

        $posts = $api->getProjects($_GET['project']);
        
        $totalPosts = count($posts);

        $title = $post->title."Projects - Minas Kosmidis - Architecture In Concept";
        //$description = $post->title." - ".$post->location.", ".$post->date."- A project by Minas Kosmidis - Architecture, Interior Design ".
        
        for ($i=0; $i < $totalPosts; $i++) {

            $data = array();

            $data['id'] = $posts[$i]->id;
            $data['title'] = $posts[$i]->title;
            $data['link'] = $posts[$i]->link;
            $data['thumbnail'] = $posts[$i]->thumbnail;

            $seoMarkup.= "<a href=\"".$posts[$i]->link."\">";
            $seoMarkup.= "<img src=\"http://minaskosmidis.com/cms/wp-content/uploads/".$posts[$i]->thumbnail."\" >";
            $seoMarkup.= "<p>".$posts[$i]->title."</p>";
            $seoMarkup.= "<p>".$posts[$i]->location.", ".$posts[$i]->date."</p>";
            $seoMarkup.="</a>";

            if($posts[$i]->title !="Apartment" || $posts[$i]->title!="Single House"){
                $keywords.=$posts[$i]->title.", ";
            }

        }

       

    }

?>
