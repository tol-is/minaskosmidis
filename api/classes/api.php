<?php
/**
 * Responsible for retrieving all the necessary data from the wp database
 *
 * @class API
 * @author Apostolos Christodoulou
 * @version 1.0
 */
class API {

    /**
     * Constants
     */
     protected static $XML = '';
     protected static $DB_USERNAME = 'root';
     protected static $DB_PASSWORD = 'root';
     protected static $DB_HOST = 'localhost';
     protected static $DB_NAME = 'minaskosmidis_db';

     
     public $posts = array();


    public function getProject($projectId = null) {

        $db = new DB();
        $db->connect(self::$DB_USERNAME, self::$DB_PASSWORD, self::$DB_HOST, self::$DB_NAME);

        $query = "SELECT * FROM wp_posts
        WHERE post_status='publish'
        AND post_type='project'
        AND post_name='".$projectId."'";

        $result = mysql_query($query) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        $row = mysql_fetch_row($result, MYSQL_ASSOC);

        $post = $this->createFullProject($row);
        
        
        unset($result);

        $db->disconnect();
        unset($db);

        return $post;

    }

    public function getProjects() {

        $posts = array();
        $db = new DB();
        $db->connect(self::$DB_USERNAME, self::$DB_PASSWORD, self::$DB_HOST, self::$DB_NAME);

        $query = "SELECT * FROM wp_posts
        WHERE post_status='publish'
        AND post_type='project'";

        $result = mysql_query($query) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        while($row = mysql_fetch_array($result, MYSQL_ASSOC))
        {
            $post = $this->createListProject($row);
            $posts[] = $post;
                       
        }
        
        unset($result);

        $db->disconnect();
        unset($db);

        return $posts;

    }


    protected function createFullProject($row) {

        $post = new Project($row);

        // Get the attachments
        $queryAt = "SELECT * FROM wp_postmeta
        WHERE post_id=".$row['ID'];

        $resultAt = mysql_query($queryAt) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        while($rowAt = mysql_fetch_array($resultAt, MYSQL_ASSOC)){

            

            if($rowAt['meta_key'] === 'project_thumb') {

                $queryThumbnail = "SELECT * FROM wp_postmeta
                WHERE post_id=" . $rowAt['meta_value'];

                $resultTh = mysql_query($queryThumbnail) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());

                while($rowTh = mysql_fetch_array($resultTh, MYSQL_ASSOC)){

                    if($rowTh['meta_key'] === '_wp_attached_file') {
                        $post->thumbnail = $rowTh['meta_value'];
                    }
                }

            }
            else if($rowAt['meta_key']==="project_title"){
                $post->title = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'project_location') {
                $post->location = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'project_date') {
                $post->date = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'project_gallery') {

                $gallery = unserialize($rowAt['meta_value']);

                foreach($gallery as $img ){
                    
                    $queryimage = "SELECT meta_value FROM wp_postmeta
                                WHERE meta_key='_wp_attachment_metadata'
                                AND post_id='".$img."'";


                    $resultImg = mysql_query($queryimage) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
                    while($rowImg = mysql_fetch_array($resultImg, MYSQL_ASSOC)){

                        $imageobject = unserialize($rowImg['meta_value']);

                        $file = $imageobject["file"];
                        $path = rtrim($file, '/');
                        $path = explode('/', $path);

                        $post->gallery[] = $path[0]."/".$path[1]."/".$imageobject["sizes"]["thumbnail"][file];
                    }

                }
                
            }




            
        }

        unset($resultAt);
        //print_r($post);
        return $post;
    }

    protected function createListProject($row) {

        $post = new Project($row);

        // Get the attachments
        $queryAt = "SELECT * FROM wp_postmeta
        WHERE post_id=".$row['ID'];

        $resultAt = mysql_query($queryAt) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        while($rowAt = mysql_fetch_array($resultAt, MYSQL_ASSOC)){

            if($rowAt['meta_key'] === 'project_thumb') {

                $queryThumbnail = "SELECT * FROM wp_postmeta
                WHERE post_id=" . $rowAt['meta_value'];

                $resultTh = mysql_query($queryThumbnail) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());

                while($rowTh = mysql_fetch_array($resultTh, MYSQL_ASSOC)){

                    if($rowTh['meta_key'] === '_wp_attached_file') {
                        $post->thumbnail = $rowTh['meta_value'];
                    }
                }

            }
            else if($rowAt['meta_key']==="project_title"){
                $post->title = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'project_location') {
                $post->location = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'project_date') {
                $post->date = $rowAt['meta_value'];
            }
            
        }

        unset($resultAt);
        
        return $post;
    }

    

}
?>
