<?php
/**
 * The post class is responsible for parsing the data of a project
 *
 * @class Project
 * @author Apostolos Christodoulou
 * @version 1.0
 */
class Project {

    /**
     * Constants
     */
    public static $SORT_DATE_ASC = 'post_date ASC';
    public static $SORT_DATE_DESC = 'post_date DESC';

    /**
     * Public variables
     */
    public $id = 0;
    public $title = '';
    public $link = '';
    public $location = '';
    public $date = '';
    public $thumbnail = '';
    public $gallery = array();
    

    /**
     * Constructor
     * @param {Array} $object The object that we need to parse
     * @author Thodoris Tsiridis
     */
    public function __construct($object) {
        $this->id = $object['ID'];
        $this->link = "http://minaskosmidis.com/project/".$object['post_name'];

    }

    public function addMeta($key, $value){
        $this->meta[$key] = $value;
    }

    
}
?>
