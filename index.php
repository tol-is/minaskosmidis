<?php
	
	include("api/seo.php");

?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $title;?></title>
	<meta name="description" content="<?php echo $description;?>" />
	<meta name="keywords" content="<?php echo $keywords;?>" />
	<meta name="author" content="num42.com" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.6, minimum-scale=0.5">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	<link rel="icon" href="favicon.ico" />
	<link rel="author" href="/humans.txt" />
	<link rel="image_src" href="<?php echo $imageSrc;?>" /> 

	<link rel="stylesheet" href="http://minaskosmidis.com/css/ie.css">
	<link rel="stylesheet" href="http://f.fontdeck.com/s/css/nkml9nBXVzel2sQrKfVDMmkFjBk/minaskosmidis.com/23832.css" type="text/css" />
	<link rel="stylesheet" href="http://minaskosmidis.com/css/style.css">
	<script data-main="/js/main" src="/js/libs/require/require.js"></script>
	
	<!--[if IE]>
	<script src="/js/libs/ie.js"></script>
	<![endif]-->

</head>
<body>

	<div id="main">
			<div id="header" class="container">
			
				<div class="grid_4"></div>
		
				<div class="grid_4" id="logo">
					<img src="/images/logo.png">
				</div>
	
				<div class="grid_4"></div>


				<div class="grid_12" id="menu">
					
					<div id="mainmenu">
					
						<span id="news_btn" class="menu-item active" data-slug="news">
							<a href="/news">NEWS</a>
						</span>
						
						<span id="projects_btn" class="menu-item" data-slug="projects">
							<a href="/projects">PROJECTS</a>
						</span>
						
						<span id="publications_btn" class="menu-item" data-slug="publications">
							<a href="/publications">PUBLICATIONS</a>
						</span>
						
						<span id="studio_btn" class="menu-item" data-slug="studio">
							<a href="/studio">STUDIO</a>
						</span>

					</div>

					<div class="clearfix"></div>

					<div id="categoriesmenu"></div>

					<div class="clearfix"></div>
				
				</div>
	
				<div class="clearfix"></div>

			</div>

	</div>
	<div id="seo"> <?php echo $seoMarkup; ?> </div>
	</div>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=472213769480112";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
