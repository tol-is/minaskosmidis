require.config({
  paths: {
    jquery: 'libs/jquery/jquery-min',
    underscore: 'libs/underscore/underscore-min',
    backbone: 'libs/backbone/backbone-min',
    modernizr: 'libs/modernizr/modernizr-min',
    text: 'libs/require/text',
    templates: '../templates',
    plugins: 'plugins'
  }

});

/*
 * START THE APP
 */
    
    require([
      'views/app',
      'router',
      'plugins'
    ], function(AppView, AppRouter, Plugins){
    
        Plugins.initialize();

        $.Scroll = ($.browser.mozilla || $.browser.msie) ? $('html') : $.Body;

        var appView = new AppView();
        appView.render();

        var router = new AppRouter();
        //Backbone.history.start();
        Backbone.history.start({pushState: true});



});
