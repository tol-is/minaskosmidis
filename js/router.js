define([
    'jquery',
    'underscore',
    'backbone',
    'events'
], function($, _, Backbone, Events) {
    var AppRouter = Backbone.Router.extend({
        
        routes:{
            "news": "news",    
            "news/": "news",    
            "studio": "studio",    
            "studio/": "studio",    
            "project/:projectid"   : "singleproject",
            "project/:projectid/"  : "singleproject",
            "/project/:projectid"  : "singleproject",
            "/project/:projectid/" : "singleproject",
            "projects": "projects",
            "projects/": "projects",
            "publications": "publications",
            "publications/": "publications",
            "*path":  "defaultRoute"
        },

        defaultRoute: function(){
            Events.trigger("changeView" ,{"action":"home"});
        },

        news: function(){
            Events.trigger("changeView" ,{"action":"news"});
        },

        projects: function(){
            Events.trigger("changeView" ,{"action":"projects"});
        },

        singleproject: function(projectid){
            Events.trigger("changeView" ,{"action":"singleproject", "projectid":projectid});
        },

        publications: function(){
            Events.trigger("changeView" ,{"action":"publications"});
        },

        studio: function(){
            Events.trigger("changeView" ,{"action":"studio"});
        }
    
    });

    return AppRouter;

});
