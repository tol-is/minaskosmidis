define([
  'underscore',
  'backbone'
], function(_, Backbone) {
  
  var StudioItemModel = Backbone.Model.extend({
    
    initialize: function()
    {

    }

  });
  
  return StudioItemModel;

});
