define([
  'underscore',
  'backbone'
], function(_, Backbone) {
  
  var NewsItemModel = Backbone.Model.extend({
    
    defaults:
    {
		id: 0,
		rendered: false,
		title: "",
		permalink: "",
		external_link:"",
		content:[],
		visited: false
	},

    initialize: function()
    {

    },

    toggleRendered: function()
	{
		this.set({
			rendered: !this.get("rendered")
		});
	},

	toggleVisited: function()
	{
		this.set({
			visited: !this.get("visited")
		});
	},

	isVisited:function(){
		if(localStorage.getItem("news_item_"+this.get("id")) == "1")
		{
			return true;
		}else
		{
			return false;
		}		
	},

	setVisited:function()
	{
		this.set({
			visited:true
		});
		
		localStorage.setItem("news_item_"+this.get("id"), "1");	
	}

  });
  
  return NewsItemModel;

});
