define([
  'underscore',
  'backbone'
], function(_, Backbone) {
  
  var PublicationItemModel = Backbone.Model.extend({
    
    defaults:
    {
		visited: false
	},

	initialize: function()
    {

    },

    toggleVisited: function()
	{
		this.set({
			visited: !this.get("visited")
		});
	},

	isVisited:function(){
		if(localStorage.getItem("pub_item_"+this.get("id")) == "1")
		{
			return true;
		}else
		{
			return false;
		}		
	},

	setVisited:function()
	{
		this.set({
			visited:true
		});
		
		localStorage.setItem("pub_item_"+this.get("id"), "1");	
	}

  });
  
  return PublicationItemModel;

});
