define([
  'jquery',
  'underscore',
  'backbone',
  'models/publicationitem'
], function($, _, Backbone, PublicationItemModel){
  
    var PublicationsCollection = Backbone.Collection.extend({
        
        id      : "publications",
        model   : PublicationItemModel,
        
        initialize: function()
        {
            return this;
        },

        url: function () 
        {
            return 'http://minaskosmidis.com/cms/publications';
        },

        parse: function(response)
        {      
            return response.publications;
        }

    });

    return PublicationsCollection;

});