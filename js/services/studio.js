define([
    'jquery',
    'underscore',
    'backbone',
    'models/studioitem'
], function($, _, Backbone, StudioItemModel){
  
    var StudioCollection = Backbone.Collection.extend({
    
      	id       : "studio",
      	model    : StudioItemModel,
        
        initialize: function()
        {

        },

        url: function () 
      	{
        	return 'http://minaskosmidis.com/cms/studio';
      	},

      	parse: function(response)
      	{   
      		return response.grid;
      	}

    });

    return StudioCollection;

});