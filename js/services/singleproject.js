define([
  'jquery',
  'underscore',
  'backbone',
  'models/projectitem'
], function($, _, Backbone, ProjectModel){
  
	var SingleProjectCollection = Backbone.Collection.extend({
	    
		id 				: "singleproject",
		project_id 		: "",
		model 			: ProjectModel,
	    
	    initialize: function()
	    {

	    },

	    setProjectId:function(param)
		{
			this.project_id = param;
		},

	    url: function() 
		{
	  		return 'http://minaskosmidis.com/cms/project/' + this.project_id+'';
		},

		parse: function(response)
		{   
			return response;
		}
	});

	return SingleProjectCollection;

});