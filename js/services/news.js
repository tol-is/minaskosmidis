define([
  'jquery',
  'underscore',
  'backbone',
  'models/newsitem'
], function($, _, Backbone, NewsItemModel){
  
	var NewsCollection = Backbone.Collection.extend({
	    
		id 				: "news",
		current_page 	: 1,
		max_pages 		: 1,
		model 			: NewsItemModel,
	    
	    initialize: function()
	    {

	    },

	    url: function () 
		{
	  		return 'http://minaskosmidis.com/cms/page/' + this.current_page+'';
		},

		advancePage:function()
		{
			this.current_page++;
		},

		rendered: function()
		{
			return this.filter(function( news )
			{
				return news.get('rendered');
			});
		},

		notRendered: function()
		{
			return this.without.apply( this, this.rendered() );
		},

		parse: function(response)
		{   
			this.current_page = response.page;
			this.max_pages = response.maxpages;

			return response.rows;
		}
	});

	return NewsCollection;

});