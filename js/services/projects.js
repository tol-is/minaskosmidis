define([
  'jquery',
  'underscore',
  'backbone',
  'models/projectitem'
], function($, _, Backbone, ProjectItemModel){
  
    var ProjectsCollection = Backbone.Collection.extend({
        
        id              : "projects",
        categories      : [],
        model           : ProjectItemModel,
        
        initialize: function()
        {
            return this;
        },

        url: function () 
        {
            return 'http://minaskosmidis.com/cms/projects';
        },

        parse: function(response)
        {   
            this.categories = response.categories;

            return response.projects;
        }

    });

    return ProjectsCollection;

});