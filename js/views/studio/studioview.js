define([
	'jquery',
	'underscore',
	'backbone',
	'events',
	'services/studio',
 	'views/base/sectionview',
 	'views/ui/grid/gridimagebox',
 	'views/ui/grid/gridcopybox',
 	'views/ui/grid/gridquotebox',
 	'views/ui/grid/gridpushbox',
 	'views/ui/grid/gridlinebreak',
 	'views/ui/grid/gridredbox',
	'views/ui/grid/gridslideshowbox'
], function($, _, Backbone, Events, StudioCollection, SectionView, GridImageBox, GridCopyBox, GridQuoteBox, GridPushBox, GridLineBreak, GridRedBox, GridSlideshowBox){
    
    var StudioView = SectionView.extend({
    
		el: '#studiocontent',

		initialize: function() 
		{
           	SectionView.prototype.initialize.call(this);

           	this.cid="studio";

           	this.children = [];

           	this.allowAction= false;

           	this.collection = new StudioCollection();
		},

		setMobile: function()
  		{
    		SectionView.prototype.setMobile.call(this);

  			this.$el.find(".grid_push_box").hide();
  			this.$el.find(".grid_line_break").hide();
  		},

  		setTablet: function()
  		{
  			SectionView.prototype.setTablet.call(this);

  			this.$el.find(".grid_box").show();
  		},

  		setDesktop: function()
  		{
  			SectionView.prototype.setDesktop.call(this);

  			this.$el.find(".grid_box").show();
  		},

		onData:function()
		{
			SectionView.prototype.onData.call(this);

			_.each( this.collection.models, this.addOne, this );

			this.$el.append("<div class=\"clearfix\"></div>")

			this.showChildren();

		},

		addOne:function(studioitem)
		{
			var newStudioItem = this.gridContentFactory(studioitem);

			this.$el.append(newStudioItem.render().el);

			this.children.push(newStudioItem);
		},

		gridContentFactory:function(data)
		{
			var type = data.get("type");

			switch(true)
			{
				case type=="grid_copy_box":
				 	return  new GridCopyBox({ model: new Backbone.Model(data) });
				break;

				case type=="grid_image_box":
				 	return  new GridImageBox({ model: new Backbone.Model(data) });
				break;

				case type=="grid_push_box":
				 	return  new GridPushBox({ model: new Backbone.Model(data) });
				break;

				case type=="grid_line_break":
				 	return  new GridLineBreak({ model: new Backbone.Model(data) });
				break;

				case type=="grid_red_box":
				 	return  new GridRedBox({ model: new Backbone.Model(data) });
				break;

				case type=="grid_slideshow_box":
				 	return  new GridSlideshowBox({ model: new Backbone.Model(data) });
				break;

				case type=="grid_quote_box":
				 	return  new GridQuoteBox({ model: new Backbone.Model(data) });
				break;				
			}
		},

		activateChildren:function()
		{
			this.allowAction = true;
		},

		deactivateChildren:function()
		{
			this.allowAction = false;
		}
  })

  return StudioView;

});