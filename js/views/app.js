define([
    'jquery',
    'underscore',
    'backbone',
    'events',
    'views/base/sectionview',
    'views/viewstack',
    'views/ui/header',
    'views/ui/footer',
    'text!templates/layout.html'
], function($, _, Backbone, Events, SectionView, ViewStack, HeaderView, FooterView, layoutTemplate){
      
    var AppView = SectionView.extend({
    
        el              : '#main',
        viewstack       : null,
        header          : null,
        footer          : null,

        initialize: function ()
        {
            SectionView.prototype.initialize.call(this);

            var me = this;

            this.viewstack = new ViewStack();

            this.scroll = ($.browser.mozilla || $.browser.msie) ? $('html') : $("body");

            Events.on("scrollTo", this.scrollTo, this);

            Events.on("changeView", this.onChangeView, this);
        },

        render: function()
        {
            SectionView.prototype.render.call(this);

            this.$el.html(layoutTemplate);

            this.header = new HeaderView();

            this.footer = new FooterView();

            this.children.push(this.header);
            this.children.push(this.viewstack);
            this.children.push(this.footer);

            //this.addScrollListener();

        },

        onChangeView: function(event)
        {
            var action = event.action.replace(" ","");
            
            if(event.projectid)
            {
                var projectid = event.projectid.replace(" ","");    
            }
            

            this.header.setSelected(action);




            switch(true)
            {
                case action === "default" || action==="home":
                    document.title = "Minas Kosmidis - Architecture in Concept";
                    this.viewstack.setToDefault();
                break;

                case action === "news":
                    document.title = "News - Minas Kosmidis - Architecture in Concept";
                    this.viewstack.setToDefault();
                break;

                case action === "projects":
                    document.title = "Projects - Minas Kosmidis - Architecture in Concept";
                    this.viewstack.setToProjects();
                break;

                case action === "singleproject":
                    this.viewstack.setToSingleProject(projectid);
                break;

                case action === "publications":
                    document.title = "Publications - Minas Kosmidis - Architecture in Concept";
                    this.viewstack.setToPublications();
                break;

                case action === "studio":
                    document.title = "Studio - Minas Kosmidis - Architecture in Concept";
                    this.viewstack.setToStudio();
                break;
            }
        },

        scrollTo:function(event)
        {
            if(this.scroll.scrollTop() == Math.floor(event.value)) return;
            
                _opt = {duration: 1000,easing: "easeInOutExpo"};

                this.scroll.animate({scrollTop:Math.floor(event.value)},_opt);
            
        },

        addScrollListener:function()
        {
            $(window).on("scroll",_.bind(this.onScroll, this));
        },

        onScroll:function(event)
        {
            if($(window).scrollTop()>135)
            {
                this.header.setSticky();
            }else
            {
                this.header.unsetSticky();
            }
        },

    });

    return AppView;
});
