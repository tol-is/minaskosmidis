define([
  'jquery',
  'underscore',
  'backbone',
  'events',
  'views/base/baseview'
], function($, _, Backbone, Events, BaseView){
    
    ProjectThumbView = BaseView.extend({
    
    	tagName			: 'div',
		allowAction		: true,
		image 			: {},

		events: {
  			"click" : "click"
		},

		initialize: function() 
		{
           	BaseView.prototype.initialize.call(this);

           	var me = this;

           	this.model.on( 'change:visited', this.changedToVisited, this );

           	this.image = new Image();

           	this.image.onload = _.debounce(function(e)
  								{
  									me.renderComplete();      
  				
								}, 200); 
		},

		render:function()
		{
			if(this.model.isVisited())
			{
				this.changedToVisited();
			}

			var categories = _.pluck(this.model.get("categories"), 'slug');
			
			this.$el
				.attr("id", "project_thumb_"+this.model.get("permalink"))
				.addClass("grid_4 project_thumb")
			
				.append("<div class=\"project_thumb_image\"></div>")				
				.append("<div class=\"project_thumb_info_title\">"+this.model.get("project_title")+ "</div>")
				.append("<div class=\"project_thumb_info_desc\">"+this.model.get("project_location") + " " + this.model.get("project_date") + "</div>")
  				
  				.find(".project_thumb_image").append(this.image)
  				
  				.append("<div class=\"clearfix\"></div>");

	  			_.each( categories, function(value)
	  								{
	  									this.$el.addClass(value);
	  								}, this );


			var thumb = this.model.get("thumb");
			
			this.image.src = thumb[0];
			this.image.width = thumb[1];
			this.image.height = thumb[2];

			this.toggleDevice();

			return this;
		},

		click:function()
		{
			Backbone.history.navigate('/project/' + this.model.get("permalink"), true); 

			this.model.setVisited();
		},

		toggleDevice:function()
		{	
			switch(true)
			{
				case this.options.device=="mobile":
				 	this.setMobile();
				break;

				case this.options.device=="tablet":
				 	this.setTablet();
				break;

				case this.options.device=="desktop":
				 	this.setDesktop();
				break;
			}
		},

		changedToVisited: function()
		{
			this.$el.addClass("visited");
		},

		filter:function(newCategories)
		{
			var me = this;
			var foundCategories = _.filter(newCategories, function(cat)
														{ 
															return me.$el.hasClass(cat);
														});

			foundCategories.length>0 ? this.show() : this.hide();
		},

		hasAnyClass:function(classes)
		{
			var me = this;

			var foundCategories = _.filter(classes, function(aclass)
														{ 
															return me.$el.hasClass(aclass);
														});

			return foundCategories.length>0;
		},

		activate:function()
		{
			this.allowAction = true;
		},

		deactivate:function()
		{
			this.allowAction = false;
		},

		show:function()
		{
			BaseView.prototype.show.call(this);
			this.$el.fadeIn(0);
		},

		hide:function()
		{
			BaseView.prototype.hide.call(this);
			this.$el.fadeOut(0);
		}

  })

  return ProjectThumbView;
});
