define([
  'jquery',
  'underscore',
  'backbone',
  'events',
  'views/base/baseview'
], function($, _, Backbone, Events, BaseView){
    
    CategoriesMenu = BaseView.extend({
    
    	el						: '#categoriesmenu',
		selectedCategories 		: [],
		allowAction				: true,
		firstClick				: true,

		events: {
  			"click .submenu-item" : "click"
		},

		initialize: function()
		{
			BaseView.prototype.initialize.call(this);

			this.render();
		},

		render:function()
		{
			var me = this;

			_.each( this.model.get("rows"), this.addSubMenuButton, this );

			this.$el.append("<div class=\"clearfix\"></div>");

			this.toggleDevice(this.device);

			this.initCategoriesMenu();

			return this;
		},

		addSubMenuButton:function(catobj, order)
		{
			this.$el.append("<span class=\"submenu-item\" data-slug=\""+catobj.slug+"\">"+catobj.name.toUpperCase()+"</span>");

			if((order+1)/3===1)
			{
				this.$el.append("<div class=\"mobile-breaker\"></div>");
			}
		},

		initCategoriesMenu:function()
		{
			this.$el.find(".submenu-item").addClass("selected");
		},

		click:function(e)
		{
			if(!this.allowAction) return;

			var $target = $(e.currentTarget);

			if($target.hasClass("selected") && !this.firstClick) return;

			firstClick = false;

			this.$el.find(".submenu-item.selected").removeClass("selected");

			$target.addClass("selected");

			/*
			this was to allow multiple selected categories

			if($target.hasClass("selected") && this.$el.find(".selected").length==1) return;

			$target.hasClass("selected") ? $target.removeClass("selected") : $target.addClass("selected");
			*/
			
			this.dispatchSelectedCategories();
		},

		dispatchSelectedCategories: function()
		{
			var selectedCategories = [];

			this.$el.find(".submenu-item.selected").each(function(order,item){
				selectedCategories.push($(this).data("slug"));
			});

			this.selectedCategories = selectedCategories;

			Events.trigger("categoriesMenuClick" ,{"categories":this.selectedCategories});

		},

		activate:function()
		{
			this.allowAction = true;
		},

		deactivate:function()
		{
			this.allowAction = false;
		}

  })

  return CategoriesMenu;
});
