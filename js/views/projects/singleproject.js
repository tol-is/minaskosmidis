define([
	'jquery',
	'underscore',
	'backbone',
	'events',
	'services/singleproject',
 	'views/base/sectionview',
 	'views/projects/projectslideshow',
], function($, _, Backbone, Events, SingleProjectCollection, SectionView, ProjectSlideshow){
    
    var SingleProjectView = SectionView.extend({
    
		el 	 			: '#projectscontent',
		$pel 			: {},
		offsetTop 		:0,
		currentProject	: "",
		events: {
  			"click #slideshow" : "click"
		},

		initialize: function() 
		{
           	SectionView.prototype.initialize.call(this);

           	this.cid="singleprojects";

           	this.children = [];

           	this.allowAction= false;

           	this.$pel = $("#singleproject");

           	this.$pel.addClass("hidden");

           	this.collection = new SingleProjectCollection();

           	this.render();

           	this.updateProjectId(this.options.projectid);
		},

		updateProjectId:function(param)
		{
			this.isActiveView = true;

			this.currentProject = param;

			this.collection.setProjectId(param);
			
			this.loadData();
		},

		render: function()
		{
			SectionView.prototype.render.call(this);

			this.$pel.addClass("grid_12");

			this.configSlideShow();
		},

		activateChildren:function()
		{
			this.allowAction = true;
		},

		deactivateChildren:function()
		{
			this.allowAction = false;
		},

		onResize:function()
		{
			SectionView.prototype.onResize.call(this);

			if(!this.isActiveView) return;

			this.configSlideShow();

			this.$pel.height(this.slideshowHeight + 75 + 5*this.gridMargin);

			if(this.slideshow)
				this.slideshow.resize(this.$el.width(), this.slideshowHeight,this.gridMargin*2);
		},

		configSlideShow:function()
		{
			this.slideshowHeight = $(window).height() - 198;

			this.gridMargin = this.$pel.css("margin-right").replace("px", "");

			if(this.device=="tablet")
			{
				this.slideshowHeight = 380;
				this.offsetTop = this.$pel.offset().top - 20;
			}

			if(this.device=="mobile")
			{
				this.slideshowHeight = 200;
				this.offsetTop = this.$pel.offset().top - 20;
			}

			if(this.device=="desktop")
			{
				this.gridMargin = this.$pel.css("margin-right").replace("px", "");
				this.offsetTop = this.$pel.offset().top - this.gridMargin*2;
			}

			if(this.slideshowHeight<250)
			{
				this.slideshowHeight = 250;
			}

			if(this.slideshowHeight>600)
			{
				this.slideshowHeight = 600;
			}
		},

		setViewportSize: function() 
		{
			SectionView.prototype.setViewportSize.call(this);
			
			this.setAutoWidth();
		},

		toggleChildrenDevice:function()
		{
			if(this.slideshow)
				this.slideshow.toggleDevice(this.device);
		},

		onData:function()
		{
			SectionView.prototype.onData.call(this);

			if(!this.slideshow)
			{
				this.slideshow = new ProjectSlideshow({"images": this.collection.models[0].get("gallery"),"device":this.device});
				this.children.push(this.slideshow);
				this.slideshow.render();
			}
			else
			{
				this.slideshow.updateImageData(this.collection.models[0].get("gallery"));
			}

			this.slideshow.show();

			$url = "http://minaskosmidis.com/project/"+this.currentProject;
			$('#fb_like_container').html('<div class="fb-like" data-send="false" data-layout="button_count" data-width="200" data-show-faces="false" data-action="like"></div>');
			
			FB.XFBML.parse($('#fb_like_container').get(0));

			document.title = this.collection.models[0].get("project_title")+" by Minas Kosmidis";

			var projectInfoString = "<h1>"+this.collection.models[0].get("project_title")+"</h1>"
							  + "<p>"+ this.collection.models[0].get("project_location") + " "+ this.collection.models[0].get("project_date")+"</p>";			
			this.$pel.find("#projectinfo").html(projectInfoString);

			this.$pel.append("<div class=\"clearfix\"></div>");


			this.onResize();
		},

		click:function()
		{
			if(!this.slideshow || !this.isActiveView) return;
				Events.trigger("scrollTo", {"value":this.offsetTop});
		},

		show:function()
	    {
			this.$pel.removeClass("hidden");

			this.$pel.height(this.slideshowHeight + 75 + 5*this.gridMargin);
	    	
	    	Events.trigger("scrollTo", {"value":this.offsetTop});

	    	this.activate();
	    },

	    hide:function()
	    {
	    	this.$pel.addClass("hidden");

	    	this.$pel.height(0);

	    	this.slideshow.hide();

	    	var me = this;
			
			var resetSlideshow = _.debounce(function(e)
					            {
					                me.slideshow.resetSlideshow();

					            }, 100);

            resetSlideshow();

            this.deactivate();
	    },

  })

  return SingleProjectView;

});