define([
	'jquery',
	'underscore',
	'backbone',
	'events',
	'views/base/baseview',
 	'views/ui/uiimage',
], function($, _, Backbone, Events, BaseView, UiImage){
    
    var ProjectSlideshow = BaseView.extend({
    
		el 					: '#slideshow',
		images 				: {}, 
		slideshowHeight		: 0,
		slideshowWidth 		: 0,
		slideMargin 		: 0,
		currentImage 		: 0,

		events: {
  			"mouseover #btn-left"	: "hoverOn",
  			"mouseout #btn-left"	: "hoverOff",
  			"mouseover #btn-right"	: "hoverOn",
  			"mouseout #btn-right"	: "hoverOff",
  			"keypress" 				: "onKey"
		},	

		initialize: function() 
		{           	
           	BaseView.prototype.initialize.call(this);

           	this.cid="projectslideshow";

           	this.$images = this.$el.find("#images");

           	this.children = [];

           	this.currentImage = 0;

           	this.allowAction= false;

           	this.images = this.options.images;
           	
           	this.addSwipeListeners();
		},

		addSwipeListeners:function()
		{
			var me = this;
			
			this.$el.swipe({

				swipeRight:function(event)
				{
					me.swipeRight(event);
				},

				swipeLeft:function(event)
				{
					me.swipeLeft(event);	
				},

				click: function(event)
				{
					me.click(event);
				},

				threshold:20,
				allowPageScroll:"vertical"

			});
		},

		swipeLeft:function(e)
		{	
			if(!this.allowAction) return;

			if(this.currentImage<this.children.length-1)
			{
				this.currentImage++;
				this.slideToCurrentImage();
			}
		},

		swipeRight:function(e)
		{
			if(!this.allowAction) return;

			if(this.currentImage>0)
			{
				this.currentImage--;
				this.slideToCurrentImage();
			}
		},

		click:function(e)
		{	
			if(!this.allowAction) return;

			if(e.clientX < $(window).width()*0.5)
			{
				this.swipeRight(e);
			}
			else
			{
				this.swipeLeft(e);
			}
		},

		hoverOn: function(e)
		{ 
			if(!this.allowAction) return;

			$(e.currentTarget).addClass("hover");
		},

		hoverOff: function(e)
		{
			if(!this.allowAction) return;

			$(e.currentTarget).removeClass("hover");
		},

		onKey:function(e)
		{
			log(e);
		},

		slideToCurrentImage:function()
		{
			var offset = this.children[this.currentImage].leftOffset;
			
			this.$images.css("margin-left", 0-offset);
		},

		render:function()
		{
			this.appendImages();
		},

		redraw:function()
		{
			this.toggleChildrenDevice();
		},

		updateImageData:function(newImages)
		{
			this.resetSlideshow();

			this.images = newImages;
			
			this.appendImages();
		},

		resetSlideshow:function()
		{
			this.removeAllImages();

			this.currentImage = 0;
			
			this.$images.css("margin-left", 0);
		},

		appendImages:function()
		{
			_.each(this.images, this.addOneImage, this );
 		},

		addOneImage:function(imageItem)
		{			
			var newImage = new UiImage({"imageData":imageItem, "type":"project_image", "device":this.device});
			
			this.$images.append(newImage.render().el);

			this.children.push(newImage);
		},

		removeAllImages:function()
		{
			_.invoke(this.children, 'destroy');

			this.children = [];
		},
		
		resize:function(width, height, margin)
		{
			this.slideMargin = margin;

			this.slideshowHeight = height;

			this.resizeChildren();

			this.slideshowWidth = this.getSlideshowWidth();
			
			this.$images
					.height(this.slideshowHeight)
					.width(this.slideshowWidth);

			var containerWidth = width-(this.slideMargin);
			$("#buttons")
				.width(containerWidth)
				.height(this.slideshowHeight)


			$("#btn-left")
				.width(Math.floor(containerWidth*0.5)-10)
				.height(this.slideshowHeight)

			$("#btn-right")
				.width(Math.floor(containerWidth*0.5)-10)
				.height(this.slideshowHeight)

			this.slideToCurrentImage()
		},

        toggleChildrenDevice:function()
		{
			_.invoke(this.children, 'toggleDevice', this.device);
		},

		getSlideshowWidth:function()
		{
			var newWidth = 0;

			for (var i = 0; i < this.children.length; i++)
			{
				this.children[i].setOffset(newWidth);
				newWidth += this.children[i].getWidth() + this.slideMargin;
			};
			newWidth +=1600;

			return newWidth;
		},

		resizeChildren:function()
		{
			_.invoke(this.children, 'resize', this.slideshowHeight, this.slideMargin);
		},

		show:function()
  		{
  			BaseView.prototype.show.call(this);

  			this.activate();

  			_.invoke(this.children, 'show');
  		},

  		hide:function()
  		{
  			BaseView.prototype.hide.call(this);

  			this.deactivate();

  			_.invoke(this.children, 'hide');
  		},
		
  })

  return ProjectSlideshow;

});