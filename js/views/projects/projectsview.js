define([
	'jquery',
	'underscore',
	'backbone',
	'events',
	'services/projects',
 	'views/base/sectionview',
 	'views/projects/categoriesmenu',
 	'views/projects/projectthumb',
 	'views/projects/singleproject'
], function($, _, Backbone, Events, ProjectsCollection, SectionView, CategoriesMenu, ProjectThumbView, SingleProjectView){
    
    var ProjectsView = SectionView.extend({
    
		el 					: '#projectscontent',
		singleProject 		: null,
		state				: "list",
		categories 			: {},
		categoriesMenu 		: null,
		currentProject 		: "",

		events: {
  			"mouseover .project_thumb"	: "hoverOn",
  			"mouseout .project_thumb"	: "hoverOff"
		},

		initialize: function() 
		{
           	SectionView.prototype.initialize.call(this);

           	this.cid="projects";

           	this.children = [];

           	this.allowAction= false;

           	this.collection = new ProjectsCollection();
		},

		initializeCategoriesMenu: function()
		{
			this.currentCategories = _.pluck(this.collection.categories.rows, 'slug');

			this.categoriesMenu = new CategoriesMenu({ model: new Backbone.Model(this.collection.categories) });
		},

		setToList:function()
		{
			if(this.state=="project")
			{
				this.unloadProject();
				this.currentProject="";
				this.state = "list";
			}
		},

		setToSingleProject:function(projectid)
		{
			this.state = "project";
			this.currentProject = projectid;
			this.loadProject();
		},

		updateCategories: function(newCategories)
		{
			this.currentCategories = newCategories;

			this.setToList();

			this.showChildren();
		},

		loadProject:function()
		{
			if(!this.singleProject)
			{
				this.singleProject = new SingleProjectView({"projectid":this.currentProject, "device":this.device});
			}
			else
			{
				this.singleProject.updateProjectId(this.currentProject);
			}

			this.singleProject.show();

			this.triggerProjectThumb();
		},

		unloadProject:function()
		{
			if(this.singleProject)
			{
				this.singleProject.hide();
			}
		},

		triggerProjectThumb:function()
		{
			$(".project_thumb.selected").removeClass("selected");

			$("#project_thumb_"+this.currentProject).addClass("selected");
		},

		hoverOn: function(e)
		{ 
			if(!this.allowAction) return;

			$(e.currentTarget).addClass("hover");
		},

		hoverOff: function(e)
		{
			if(!this.allowAction) return;

			$(e.currentTarget).removeClass("hover");
		},

		activate:function()
		{
			SectionView.prototype.activate.call(this);

			this.allowAction = true;
		},

		deactivate:function()
		{
			SectionView.prototype.deactivate.call(this);

			this.allowAction = false;
		},

		onData:function()
		{
			SectionView.prototype.onData.call(this);

			_.each( this.collection.models, this.addOne, this );

			this.$el.append("<div class=\"clearfix\"></div>");

			this.initializeCategoriesMenu();

			this.categoriesMenu.show();

			this.triggerProjectThumb();

			this.showChildren();
		},

		addOne:function(projectitem)
		{
			var view = new ProjectThumbView({ model: projectitem, device:this.device});

			this.$el.append(view.render().el);

			this.children.push(view);
		},

		show:function()
		{
			SectionView.prototype.show.call(this);

			if(this.categoriesMenu) this.categoriesMenu.show();
		},

		showChildren:function()
		{
			var me = this;

			var childrenToShow = [];

			if(_.indexOf(this.currentCategories, "all")>=0)
			{
				childrenToShow = this.children;
			}
			else
			{
				childrenToShow = _.filter(this.children, function(child)
														{
															return child.hasAnyClass(me.currentCategories);
														});
			}

			/*
			Might be used to animate positions
			_.each(childrenToShow, function(child, key)
									{
										
										//log(child.$el.width());
										child.show();
										//log(child);
										//log(key);
									});
			*/

			var childrenToHide = _.difference(this.children, childrenToShow);
			
			_.invoke(childrenToHide, 'hide');
			_.invoke(childrenToShow, 'show');

			

		},

		hide:function()
		{
			SectionView.prototype.hide.call(this);

			this.setToList();

			if(this.categoriesMenu) this.categoriesMenu.hide();
		}
  })

  return ProjectsView;

});