define([
  'jquery',
  'underscore',
  'backbone',
  'events',
  'views/base/baseview',
], function($, _, Backbone, Events, BaseView){
    
	var SectionView = BaseView.extend({
    
    	collection			: null,
		viewport_width		: 0,
		viewport_height		: 0,
		children 			: [],
		isLoading 			: false,
		isActiveView 		: false,
		allowAction			: false,
		addFetchedData		: false,
		itemsToLoad			: 0,
		itemsLoaded			: 0,

		initialize:function()
		{
			BaseView.prototype.initialize.call(this);
		},

		addItemRenderCompleteListener:function()
		{
			Events.on("itemRenderComplete", this.onItemRenderComplete, this);
		},

		render:function()
		{
			this.addResizeListener();
		},

		onItemRenderComplete:function()
		{	
			this.itemsLoaded++;
		
			if(this.itemsLoaded == this.itemsToLoad)
			{
				this.$el.removeClass("unloaded");

				Events.trigger("renderComplete" ,{"section":this.cid});
			}
		},

		redraw:function()
		{
			BaseView.prototype.redraw.call(this);
			
			this.toggleChildrenDevice();
		},

		addResizeListener:function()
  		{
  			var me = this; 

  			var updateLayout = _.debounce(function(e)
						        {
						          this.onResize();
						        }, 500); 

	        $(window).on("resize",_.bind(updateLayout, this));

  		},

  		onResize:function()
		{
			this.setViewportSize();

			if(this.device!="desktop") return;

      		var wid = $(window).width();
      		var hei = $(window).height();

			var newWidth = wid - 40;
			newWidth <=1600 ? this.$el.width(newWidth) : this.$el.width(1600);
		},

		setViewportSize: function() 
		{
			this.viewport_width = $(window).width();
			this.viewport_height = $(window).height();

			switch(true)
			{
				case this.viewport_width<768:
					this.toggleDevice("mobile");
					this.setAutoWidth();
				break;

				case this.viewport_width<1030:
					this.toggleDevice("tablet");
					this.setAutoWidth();
				break;

				default:
					this.toggleDevice("desktop");
			}
		},

		toggleChildrenDevice:function()
		{
			_.invoke(this.children, 'toggleDevice', this.device);
		},

		setAutoWidth:function()
		{
			this.$el.css("width", "auto");
		},

	    loadData:function()
	    {
			var me = this;

			this.isLoading = true;
 
			this.collection.fetch({ 
							
				add: this.addFetchedData,

				success:function(event)
				{
					me.onData(event);
				},
				
				error:function(event)
				{
					me.onError(event);
				}
			});
	    },

	    onData: function(event)
	    {
	    	this.itemsToLoad = this.collection.models.length;

	    	this.addItemRenderCompleteListener();

	    	this.isLoading = false;

	    },

	    onError:function(event)
	    {
	    	//show 404;
	    },

	    show:function()
  		{
  			BaseView.prototype.show.call(this);

  			this.$el.fadeIn(1200);

  			this.activate();

  			this.showChildren();

	    	$(window).trigger("resize");
  		},

  		showChildren:function()
  		{
  			_.invoke(this.children, 'show');
  		},

  		hide:function()
  		{
  			BaseView.prototype.hide.call(this);

  			this.$el.fadeOut(1200);

  			this.hideChildren();

  			this.deactivate();
  		},

  		hideChildren:function()
  		{
  			_.invoke(this.children, 'hide');
  		},

	    activate:function()
		{
			BaseView.prototype.activate.call(this);

			this.isActiveView = true;

			this.activateChildren();
		},

		deactivate:function()
		{
			BaseView.prototype.deactivate.call(this);

			this.isActiveView = false;

			this.deactivateChildren();
		},

		activateChildren:function()
		{
			_.invoke(this.children, 'activate');
		},

		deactivateChildren:function()
		{
			_.invoke(this.children, 'deactivate');
		}, 

  })

  return SectionView;
});
