define([
    'jquery',
    'underscore',
    'backbone',
    'events'
], function($, _, Backbone, Events){
    
	var BaseView = Backbone.View.extend({

        device          : '',
        allowAction     : true,

		initialize:function()
        {
            this.device = this.options.device;

            this.$el.addClass("hidden unloaded");
        },

        renderComplete:function()
        {   
            var me = this;

            var rendercomplete = _.debounce(function(e)
                                {
                                    me.$el.removeClass("unloaded");            
                
                                    Events.trigger("itemRenderComplete"); 

                                }, 100);

            rendercomplete();
        },

        toggleDevice:function(deviceparam)
        { 
            switch(true)
            {
              case deviceparam=="mobile" && this.device != "mobile":
                this.setMobile();
              break;

              case deviceparam=="tablet" && this.device != "tablet":
                this.setTablet();
              break;

              case deviceparam=="desktop" && this.device != "desktop":
                this.setDesktop();
              break;
            }
        },

        setMobile:function()
  		{
  			this.device = "mobile";
  			this.redraw();
  		},

  		setTablet:function()
  		{
  			this.device = "tablet";
  			this.redraw();
  		},

  		setDesktop:function()
  		{
  			this.device = "desktop";
  			this.redraw();
  		},

        redraw:function()
        {

        },

    	show:function()
        {
            this.$el.removeClass("hidden");
        },

        hide:function()
        {
            this.$el.addClass("hidden");
        },

        activate:function()
        {
            this.allowAction = true;
        },

        deactivate:function()
        {
            this.allowAction = false;
        },

        destroy:function(){
            this.remove();
            this.unbind();
        }

  })

  return BaseView;
});
