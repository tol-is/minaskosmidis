define([
  'jquery',
  'underscore',
  'backbone',
  'events',
  'views/base/sectionview',
  'services/news',
  'views/news/newsitem'
], function($, _, Backbone, Events, SectionView, NewsCollection, NewsItemView){
    
    var NewsView = SectionView.extend({
    
		el: '#newscontent',

		initialize: function() 
		{
           	SectionView.prototype.initialize.call(this);

           	this.cid="news";

           	this.children = [];

           	this.collection = new NewsCollection();

           	this.addFetchedData = true;
		},

	    show:function()
		{			
			SectionView.prototype.show.call(this);
			
		},

		hide:function()
		{
			SectionView.prototype.hide.call(this);

		},

		redraw:function()
		{
			SectionView.prototype.redraw.call(this);

			if(this.isActiveView==true)
			{
				this.show();
			}
			else
			{
				this.hide();
			}
		},

		onData:function()
		{
			SectionView.prototype.onData.call(this);

			this.itemsLoaded = 0;

			this.itemsToLoad = this.collection.notRendered().length;

			_.each( this.collection.notRendered(), this.addOne, this );

			this.$el.append("<div class=\"clearfix\"></div>");

			this.showChildren();

			var me = this; 

  			var updateScroll = _.debounce(function(e)
						        {
						          this.onScroll();
						        }, 500); 

	        $(window).on("scroll",_.bind(updateScroll, this));
		},

		addOne:function(newsitem)
		{			
			var view = new NewsItemView({ model: newsitem, device:this.device});

			this.$el.append(view.render().el);

			this.children.push(view);
		},

		onScroll: function (event)
		{
	      	if(!this.isActiveView) return;

	      	var triggerPoint = 300; 

	        if( !this.isLoading && $(window).scrollTop() + $(window).height() + triggerPoint >= this.$el.height() ) {
	        	this.collection.advancePage();
	          	this.loadData();
	        }
    	}
  })

  return NewsView;

});