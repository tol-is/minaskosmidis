define([
  'jquery',
  'underscore',
  'backbone',
  'events',
  'views/base/baseview',
  'views/news/newsrenderers/newsimagebox',
  'views/news/newsrenderers/newscaptionbox',
  'views/news/newsrenderers/newsquotebox',
  'views/news/newsrenderers/newspushbox',
  'views/news/newsrenderers/newslinebreak'
], function($, _, Backbone, Events, BaseView, NewsImageBox, NewsCaptionBox, NewsQuoteBox, NewsPushBox, NewsLineBreak){
    
    NewsItemView = BaseView.extend({
    
    	tagName 		: 'div',
		allowAction		: true,
		images			: [],
		itemsToLoad		: 0,
		itemsLoaded		: 0,

		events: {
  			"mouseover .sub_content_item"	: "hoverOn",
  			"mouseout .sub_content_item"	: "hoverOff",
  			"click .sub_content_item"		: "click"
		},

		initialize: function() 
		{
			BaseView.prototype.initialize.call(this);
			
			this.children = [];
			
			this.images = [];
           	
           	this.model.on( 'change:visited', this.changedToVisited, this );
		},

		addItemRenderCompleteListener:function()
		{
			Events.on("newsItemRenderComplete", this.itemRenderComplete, this);
		},

		render:function()
		{
			this.itemsToLoad = this.model.get("content").length;

			this.addItemRenderCompleteListener();

			if(this.model.isVisited()){
				this.changedToVisited();
			}

			this.$el
				.addClass("news_item")
				.addClass("story_"+this.model.get("id"));

			for (var idx = 0; idx < this.model.get("content").length; idx++)
			{
				var obj = this.model.get("content")[idx];
				
				var newsubview = this.subContentFactory(idx, obj);

				this.$el.append(newsubview.render().el);
				
				this.children.push(newsubview);
			};
			
			this.$el.append("<div class=\"clearfix\"></div>")

			this.model.toggleRendered();

			this.toggleDevice(this.device);

			return this;
		},

		itemRenderComplete:function()
		{	
			this.itemsLoaded++;
			
			if(this.itemsLoaded == this.itemsToLoad)
			{
				this.renderComplete();
			}
		},

		subContentFactory:function(order, data)
		{
			switch(true)
			{
				case data.type=="news_image_box":
				 	return new NewsImageBox({ model: new Backbone.Model(data) });
				break;

				case data.type=="news_caption_box":
					return  new NewsCaptionBox({ model: new Backbone.Model(data) });
				break;

				case data.type=="news_quote_box":
					return new NewsQuoteBox({ model: new Backbone.Model(data) });
				break;

				case data.type=="news_push_box":
					return new NewsPushBox({ model: new Backbone.Model(data) });
				break;

				case data.type=="news_line_break":
					return new NewsLineBreak({ model: new Backbone.Model(data) });
				break;
			}
			
		},

		setMobile: function()
  		{
    		BaseView.prototype.setMobile.call(this);

    		this.$el
    			.find('.news_image_box, .news_caption_box, .news_quote_box, .news_push_box, .news_line_break')
    			.hide();

  			this.$el.find(".news_image_box").eq(0).show();
			this.$el.find(".news_quote_box").eq(0).show();
			this.$el.find(".news_caption_box").eq(0).show();
  		},

  		setTablet: function()
  		{
  			BaseView.prototype.setTablet.call(this);

  			this.$el.find(".sub_content_item").show();
  		},

  		setDesktop: function()
  		{
  			BaseView.prototype.setDesktop.call(this);

  			this.$el.find(".sub_content_item").show();
  		},

  		hoverOn: function(e)
		{ 
			if(!this.allowAction) return;

			this.$el.addClass("hover");
		},

		hoverOff: function(e)
		{
			if(!this.allowAction) return;

			this.$el.removeClass("hover");
		},

		click:function(e)
		{
			if(!this.allowAction) return;

			if(this.model.get("external_link").length>7 && this.model.get("external_link")!="http://")
			{
				window.open(this.model.get("external_link"), '_blank').blur();
  				//window.focus();
			}else
			{
				Backbone.history.navigate(this.model.get("permalink"), true); 
			}

			this.model.setVisited();
		},

		changedToVisited: function()
		{
			this.$el.addClass("visited");
		},

		activate:function()
		{
			this.allowAction = true;
		},

		deactivate:function()
		{
			this.allowAction = false;
		},

  })

  return NewsItemView;
});
