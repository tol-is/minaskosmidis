define([
  'jquery',
  'underscore',
  'backbone',
  'events',
  'views/base/baseview'
], function($, _, Backbone, Events, BaseView){

  var BaseNewsRenderer = BaseView.extend({

		tagName			: 'div',
		type			: '',

		render:function()
		{
			this.type = this.model.get("type");

			this.$el
				.addClass(this.model.get("type"))
				.addClass("sub_content_item");
		},

		renderComplete:function()
        {   
            var me = this;

            var rendercomplete = _.debounce(function(e)
					            {
					                me.$el.removeClass("unloaded");            
					                
					                Events.trigger("newsItemRenderComplete"); 

					            }, 100);

            rendercomplete();
        }
		
	});

  return BaseNewsRenderer;

});