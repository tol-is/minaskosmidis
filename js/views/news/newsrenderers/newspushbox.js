define([
  'jquery',
  'underscore',
  'backbone',
  'views/news/newsrenderers/basenewsrenderer'
], function($, _, Backbone, BaseNewsRenderer){

  var NewsPushBox = BaseNewsRenderer.extend({

		render:function()
		{
			BaseNewsRenderer.prototype.render.call(this);

			this.$el.addClass("grid_"+parseInt(this.model.get("push_width")));

			this.renderComplete();

			return this;
		}
		
	});

  return NewsPushBox;
  
});