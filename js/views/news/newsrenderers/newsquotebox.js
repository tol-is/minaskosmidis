define([
  'jquery',
  'underscore',
  'backbone',
  'views/news/newsrenderers/basenewsrenderer'
], function($, _, Backbone, BaseNewsRenderer){

  var NewsQuoteBox = BaseNewsRenderer.extend({

		render:function()
		{
			BaseNewsRenderer.prototype.render.call(this);

			this.$el
				.append(""+this.model.get("quote_copy")+"")
				.addClass("grid_"+parseInt(this.model.get("quote_width")));

			this.renderComplete();

			return this;
		}
		
	});

  return NewsQuoteBox;
  
});
