define([
  'jquery',
  'underscore',
  'backbone',
  'views/news/newsrenderers/basenewsrenderer'
], function($, _, Backbone, BaseNewsRenderer){

  var NewsLineBreak = BaseNewsRenderer.extend({

		render:function()
		{
			BaseNewsRenderer.prototype.render.call(this);

			this.renderComplete();

			return this;
		}
		
	});

  return NewsLineBreak;
  
});