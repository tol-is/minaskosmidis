define([
  'jquery',
  'underscore',
  'backbone',
  'views/news/newsrenderers/basenewsrenderer'
], function($, _, Backbone, BaseNewsRenderer){

  var NewsCaptionBox = BaseNewsRenderer.extend({

		render:function()
		{
			BaseNewsRenderer.prototype.render.call(this);

			this.$el
				.append(this.model.get("caption_copy"))
				.addClass("grid_"+parseInt(this.model.get("caption_width")))
				.addClass("caption_pos_"+this.model.get("caption_pos"));

			this.renderComplete();
			
			return this;
		}
		
	});

  return NewsCaptionBox;
  
});