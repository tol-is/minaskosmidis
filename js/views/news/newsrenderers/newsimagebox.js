define([
  'jquery',
  'underscore',
  'backbone',
  'views/news/newsrenderers/basenewsrenderer'
], function($, _, Backbone, BaseNewsRenderer){

  var NewsImageBox = BaseNewsRenderer.extend({

  		image 		: {},

  		initialize: function()
  		{
  			var me = this;

  			this.image = new Image();

  			this.image.onload = _.debounce(function(e)
  			{
  				me.renderComplete();      
  				
			}, 200); 			
  		},

		render:function()
		{
			BaseNewsRenderer.prototype.render.call(this);
			
  			var imgW =  this.model.get("image_object").sizes.thumbnail[1];
			var imgH =  this.model.get("image_object").sizes.thumbnail[2];
			
			this.image.src = "";
			this.image.width = imgW;
			this.image.height = imgH;

			this.$el
				.append("<div class=\"sub_image\"></div>")
				.addClass("grid_"+parseInt(this.model.get("image_width")))
  				.find(".sub_image").append(this.image);
			
			this.loadImage(this.model.get("image_object").sizes.large);

			return this;
		},

		loadImage:function(imageObject)
		{
			var imgSrc = imageObject[0];
  			var imgW =  imageObject[1];
			var imgH =  imageObject[2];

			this.image.src = imgSrc;
			this.image.width = imgW;
			this.image.height = imgH;
		}
		
	});

  return NewsImageBox;
  
});