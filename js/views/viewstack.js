define([
	'jquery',
	'underscore',
	'backbone',
	'events',
	'views/base/baseview',
	'views/news/newsview',
	'views/projects/projectsview',
	'views/projects/singleproject',
	'views/publications/pubsview',
	'views/studio/studioview',
], function($, _, Backbone, Events, BaseView, NewsView, ProjectsView, SingleProjectView, PublicationsView, StudioView){

  var ViewStack = BaseView.extend({

		tagNathis			: 'div',
		currentSection		: '',
		views 				: {},
		isSingleProject		: false,

		initialize:function()
		{
			Events.on("renderComplete", this.onSectionRenderComplete, this);
			Events.on("categoriesMenuClick", this.onCategoriesMenuClick, this);
		},

		render:function()
		{

			BaseView.prototype.render.call(this);

			return this;
		},

	    onSectionRenderComplete: function(event)
        {
        	if(event.section == this.currentSection)
        	{
        		//was meant to do something with the the body height but never happened
        	}
        },

        onCategoriesMenuClick:function(event)
        {
        	if(this.currentSection!="projects" || typeof this.views["projects"] == 'undefined') return;

        	Backbone.history.navigate('/projects', true); 

        	this.views["projects"].updateCategories(event.categories);
        },

        setToDefault:function()
	    {	
	        if(typeof this.views["news"] == 'undefined')
	        {
				var newsView = new NewsView();
				newsView.render();
				newsView.loadData()
				this.views["news"] = newsView;
	        }
	        
	        this.setActiveSection("news");
	    },

	    setToProjects:function()
	    {
			if(typeof this.views["projects"] == 'undefined')
			{
				var projectsView = new ProjectsView();
				projectsView.render();
				projectsView.loadData();
				this.views["projects"] = projectsView;
	        }

	        this.setActiveSection("projects");

	        if(!this.isSingleProject)
	        	this.views["projects"].setToList();
	        
	    },

	    setToSingleProject:function(projectid)
	    {
	    	this.isSingleProject=true;
	    	
	    	this.setToProjects();

	    	this.views["projects"].setToSingleProject(projectid);

	    	
	    },

	    setToStudio:function()
	    {
			if(typeof this.views["studio"] == 'undefined')
			{
				var studioview = new StudioView();
				studioview.render();
				studioview.loadData();
				this.views["studio"] = studioview;
	        }

	        this.setActiveSection("studio");

	    },

	    setToPublications:function()
	    {
			if(typeof this.views["publications"] == 'undefined')
			{
				var pubsview = new PublicationsView();
				pubsview.render();
				pubsview.loadData();
				this.views["publications"] = pubsview;
	        }

	        this.setActiveSection("publications");

	    },

  		setActiveSection:function(newSection)
	    {	
	    	if(newSection == this.currentSection) return;

	    	if(this.currentSection) this.views[this.currentSection].hide();

	    	if(!this.isSingleProject)
	    	Events.trigger("scrollTo", {"value":0});

	    	this.views[newSection].show();

	    	this.currentSection = newSection;

	    	this.isSingleProject=false;
	    },

	    getActiveSection:function()
	    {
	    	return this.views[this.currentSection];
	    }
		
	});

  return ViewStack;
  
});