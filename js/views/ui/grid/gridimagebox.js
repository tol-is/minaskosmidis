define([
  'jquery',
  'underscore',
  'backbone',
  'views/ui/grid/basegridrenderer'
], function($, _, Backbone, BaseGridRenderer){

  var GridImageBox = BaseGridRenderer.extend({

  		image: {},

  		initialize: function()
  		{
  			var me = this;

  			this.image = new Image();

  			this.image.onload = function()
  			{ 
            	me.renderComplete();
			}
  		},

		render:function()
		{
			BaseGridRenderer.prototype.render.call(this);

  			var imgW =  this.model.get("image_object").sizes.thumbnail[1];
			var imgH =  this.model.get("image_object").sizes.thumbnail[2];

			this.$el
				.append("<div class=\"sub_image\"></div>")
				.addClass("grid_"+parseInt(this.model.get("image_width")))
  				.find(".sub_image").append(this.image);
			
			this.loadImage(this.model.get("image_object").sizes.medium);

			return this;
		},

		loadImage:function(imageObject)
		{
			var imgSrc = imageObject[0];
  			var imgW =  imageObject[1];
			var imgH =  imageObject[2];

			this.image.src = imgSrc;
			this.image.width = imgW;
			this.image.height = imgH;
		}
		
	});

  return GridImageBox;
  
});