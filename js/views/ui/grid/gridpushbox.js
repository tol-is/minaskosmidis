define([
  'jquery',
  'underscore',
  'backbone',
  'views/ui/grid/basegridrenderer'
], function($, _, Backbone, BaseGridRenderer){

  var GridPushBox = BaseGridRenderer.extend({

		render:function()
		{
			BaseGridRenderer.prototype.render.call(this);

			this.$el.addClass("grid_"+parseInt(this.model.get("push_width")));

			this.renderComplete();

			return this;
		}
		
	});

  return GridPushBox;
  
});