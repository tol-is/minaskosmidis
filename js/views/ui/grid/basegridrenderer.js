define([
  'jquery',
  'underscore',
  'backbone',
  'views/base/baseview'
], function($, _, Backbone, BaseView){

  var BaseGridRenderer = BaseView.extend({
	
		tagName			: 'div',
		type			: '',


		render:function()
		{
			this.type = this.model.get("type");

			this.$el
				.addClass(this.model.get("type"))
				.addClass("grid_box");
		}


		
	});

  return BaseGridRenderer;

});