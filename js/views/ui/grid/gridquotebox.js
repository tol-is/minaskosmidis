define([
  'jquery',
  'underscore',
  'backbone',
  'views/ui/grid/basegridrenderer'
], function($, _, Backbone, BaseGridRenderer){

  var GridQuoteBox = BaseGridRenderer.extend({

		render:function()
		{
			BaseGridRenderer.prototype.render.call(this);

			this.$el
				.append(""+this.model.get("quote_copy")+"")
				.addClass("grid_"+parseInt(this.model.get("quote_width")));
			
			this.renderComplete();

			return this;
		}
		
	});

  return GridQuoteBox;
  
});