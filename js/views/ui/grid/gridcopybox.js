define([
  'jquery',
  'underscore',
  'backbone',
  'views/ui/grid/basegridrenderer'
], function($, _, Backbone, BaseGridRenderer){

  var GridCopyBox = BaseGridRenderer.extend({

		render:function()
		{
			BaseGridRenderer.prototype.render.call(this);

			this.$el
				.append(this.model.get("copy"))
				.addClass("grid_"+parseInt(this.model.get("copy_width")))
				.addClass("copypos_"+this.model.get("copy_pos"));

			this.renderComplete();
			
			return this;
		}
		
	});

  return GridCopyBox;
  
});