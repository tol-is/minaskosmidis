define([
  'jquery',
  'underscore',
  'backbone',
  'views/ui/grid/basegridrenderer'
], function($, _, Backbone, BaseGridRenderer){

  var GridRedBox = BaseGridRenderer.extend({

  		initialize: function()
  		{
  		},

		render:function()
		{
			BaseGridRenderer.prototype.render.call(this);
			this.$el
				.append("<div class=\"red_box\"><img src=\"images/grid_red_box/"+this.model.get("red_box_ratio")+".png\"></div>")
				.addClass("grid_"+parseInt(this.model.get("red_box_width")))
  				.find(".sub_image").append(this.image);
			
			this.renderComplete();

			return this;
		},

		
	});

  return GridRedBox;
  
});