define([
  'jquery',
  'underscore',
  'backbone',
  'views/ui/grid/basegridrenderer'
], function($, _, Backbone, BaseGridRenderer){

	var GridLineBreak = BaseGridRenderer.extend({

		render:function()
		{
			BaseGridRenderer.prototype.render.call(this);

			this.renderComplete();

			return this;
		}
		
	});

  return GridLineBreak;
  
});