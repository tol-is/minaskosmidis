define([
    'jquery',
    'underscore',
    'backbone',
    'events',
    'views/base/baseview',
    'text!templates/footer/footer.html'
], function($, _, Backbone, Events, BaseView, footerTemplate){
  
    var FooterView = BaseView.extend({
    
        el: "#footer",
        
        initialize:function()
        {
            this.render();
            this.addResizeListener();
        },

        render: function ()
        {
          $(this.el).html(footerTemplate);
        },

        addResizeListener:function()
        {
            var me = this;

            var updateLayout = _.debounce(function(e)
            {
              this.onResize();
            }, 500); 

            $(window).on("resize",_.bind(updateLayout, this));
        },

        onResize:function()
        {
            if(this.device!="desktop")
            {
              this.setAutoWidth();
              return;
            } 
          
            var wid = $(window).width();
            var hei = $(window).height();

            var newWidth = wid - 40;
            newWidth <=1600 ? this.$el.width(newWidth) : this.$el.width(1600);
        },

        setAutoWidth:function()
        {
            this.$el.css("width", "auto");
        },

    });

  return FooterView;
});
