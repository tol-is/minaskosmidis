define([
    'jquery',
    'underscore',
    'backbone',
    'events',
    'views/base/baseview',
    'text!templates/header/header.html'
], function($, _, Backbone, Events, BaseView, headerTemplate){
    
    var HeaderView = BaseView.extend({
    
        el: "#header",

        events: {
            "click #logo"           : "logoclick",
            "click .menu-item"      : "click"
        },

        initialize:function()
        {
            this.render();
        },

        render: function ()
        {
            this.$el.html(headerTemplate);
        },

        logoclick:function(e)
        {
            Backbone.history.navigate("/", true); 
        },

        click:function(e)
        {
            var slug = $(e.target).data("slug");

            Backbone.history.navigate(slug, true); 
        },

        setSelected: function(actionId)
        {
            if(actionId === "default" || actionId==="news") actionId = "news";

            if(actionId === "projects" || actionId==="singleproject") actionId = "projects";

            $('.active').removeClass('active');

            this.$el.find("#"+actionId+"_btn").addClass('active');            
        },

        setSticky:function()
        {
            if(this.$el.hasClass("sticky")) return;

            this.$el.addClass("sticky");
        },

        unsetSticky:function()
        {
            if(!this.$el.hasClass("sticky")) return;

            this.$el.removeClass("sticky");
        }

        
  })

  return HeaderView;
});
