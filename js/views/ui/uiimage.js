define([
  'jquery',
  'underscore',
  'backbone',
  'events',
  'views/base/baseview',
], function($, _, Backbone, Events, BaseView){

  var UiImage = BaseView.extend({

		device        : '',
        type          : "",
        imageData     : {},
		image         : {},
        imageratio    :0,

        initialize: function()
        {
            BaseView.prototype.initialize.call(this);

            var me = this;

            this.type = this.options.type;
            this.imageData = this.options.imageData;
            
            this.image = new Image();

            this.image.onload = function()
            { 
                me.renderComplete();
            }
        },

        render:function()
        {
            BaseView.prototype.render.call(this);
        
            this.$el
                .addClass(this.type)
                .append(this.image);

            this.redraw();

            return this;
        },

        resize:function(height, margin)
        {
            this.$el.height(height)
                    .width(height*this.imageratio)
                    .css("marginRight", margin+"px");
        },

        getWidth:function()
        {
            return this.$el.width();
        },

        getHeight:function()
        {
            return this.$el.height();
        },

        setOffset:function(offsetparam)
        {
            this.leftOffset = offsetparam;
        },

        loadImage:function(imageObject)
        {
            var imgSrc = imageObject[0];
            var imgW =  imageObject[1];
            var imgH =  imageObject[2];

            this.imageratio = imgW/imgH;

            this.image.width = imgW;
            this.image.height = imgH;
            this.image.src = imgSrc;
        },

        redraw:function()
        {
            var imageArrray = [];
            
            if(this.device=="mobile")
            {
                imageArrray = this.imageData.sizes.medium;
            }
            else if(this.grid_size<6 )
            {
                imageArrray = this.imageData.sizes.medium;
            }
            else
            {
                imageArrray = this.imageData.sizes.large;
            }

            this.loadImage(imageArrray);
        }

		
		
	});

  return UiImage;

});