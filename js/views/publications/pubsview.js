define([
	'jquery',
	'underscore',
	'backbone',
	'events',
	'services/publications',
 	'views/base/sectionview',
 	'views/publications/pubthumb'
], function($, _, Backbone, Events, PublicationsCollection, SectionView, PublicationThumbView){
    
    var PublicationsView = SectionView.extend({
    
		el: '#pubscontent',

		events: {
  			"mouseover .pub_thumb"	: "hoverOn",
  			"mouseout .pub_thumb"	: "hoverOff"
  		},

		initialize: function() 
		{
           	SectionView.prototype.initialize.call(this);

           	this.cid="publications";

           	this.children = [];

           	this.allowAction= false;

           	this.collection = new PublicationsCollection();
		},

		hoverOn: function(e)
		{ 
			if(!this.allowAction) return;

			$(e.currentTarget).addClass("hover");
		},

		hoverOff: function(e)
		{
			if(!this.allowAction) return;

			$(e.currentTarget).removeClass("hover");
		},

		click: function(e)
		{
			if(!this.allowAction) return;

			if($(e.currentTarget).hasClass("hover"))
			{
				$(e.currentTarget).removeClass("hover");
			}
			else
			{
				$(e.currentTarget).addClass("hover");
			}
		},

		activateChildren:function()
		{
			this.allowAction = true;
		},

		deactivateChildren:function()
		{
			this.allowAction = false;
		},

		onData:function()
		{
			SectionView.prototype.onData.call(this);

			_.each( this.collection.models, this.addOne, this );

			this.$el.append("<div class=\"clearfix\"></div>");

			this.showChildren();
		},

		addOne:function(publicationitem)
		{
			var view = new PublicationThumbView({ model: publicationitem, device:this.device});

			this.$el.append(view.render().el);

			this.children.push(view);
		}
  })

  return PublicationsView;

});