define([
  'jquery',
  'underscore',
  'backbone',
  'events',
  'views/base/baseview'
], function($, _, Backbone, Events, BaseView){
    
    PublicationThumbView = BaseView.extend({
    
    	tagName 		: 'div',
		allowAction 	: true,
		image_one 		: {},
		image_two 		: {},
		imagesLoaded 	: 0,

		initialize: function() 
		{
			var me = this;

           	this.model.on( 'change:visited', this.changedToVisited, this );

           	this.image_one = new Image();
           	this.image_one.onload = function()
           	{ 
            	me.renderComplete();             
			}

           	this.image_two = new Image();
           	this.image_two.onload = function()
           	{ 
            	
            	me.renderComplete();             
			}
		},

		render:function()
		{
			if(this.model.isVisited()){
				this.setVisited();
			}

			var imageOne = this.model.get("cover");
				this.image_one.src = imageOne[0];
				this.image_one.width = imageOne[1];
				this.image_one.height = imageOne[2];

			var imageTwo = this.model.get("page");
				this.image_two.src = imageTwo[0];
				this.image_two.width = imageTwo[1];
				this.image_two.height = imageTwo[2];

			this.$el
				.attr("id", "pub_thumb_"+this.model.get("id"))
				.addClass("pub_thumb unloaded hidden")
				.addClass("grid_2")//+parseInt(this.model.get("thumb").grid_size));
				
				.append("<div class=\"pub_thumb_image_one\"></div>")
  				.find(".pub_thumb_image_one").append(this.image_one);

  			this.$el
  				.append("<div class=\"pub_thumb_image_two\"></div>")
				.find(".pub_thumb_image_two").append(this.image_two)
  				
  				.append("<div class=\"clearfix\"></div>");

  			this.$el
  				.append("<div class=\"description\"><i>"+this.model.get("date")+"</i><br>"+this.model.get("title")+"</div>");

  				//.append("")

			this.toggleDevice();

			return this;
		},

		renderComplete:function()
        {
            this.imagesLoaded++;

            if(this.imagesLoaded==2)
            {
            	Events.trigger("itemRenderComplete");
            	this.$el.removeClass("unloaded");
        	}
        },

		toggleDevice:function()
		{	
			switch(true)
			{
				case this.options.device=="mobile":
				 	this.setMobile();
				break;

				case this.options.device=="tablet":
				 	this.setTablet();
				break;

				case this.options.device=="desktop":
				 	this.setDesktop();
				break;
			}
		},

		changedToVisited: function()
		{
			this.$el.addClass("visited");
		},

		activate:function()
		{
			this.allowAction = true;
		},

		deactivate:function()
		{
			this.allowAction = false;
		}

  })

  return PublicationThumbView;
});
