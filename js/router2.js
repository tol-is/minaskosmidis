define([
    'jquery',
    'underscore',
    'backbone',
    'events'
], function($, _, Backbone, Events) {
    var AppRouter = Backbone.Router.extend({
        

        routes:{
            "news"                 : "news",    
            "news/"                : "news",    
            "projects"             : "projectspage",
            "projects/"            : "projectspage",
            
            "project/:projectid"   : "singleproject",
            "project/:projectid/"  : "singleproject",
            "/project/:projectid"  : "singleproject",
            "/project/:projectid/" : "singleproject",
            
            "studio"               : "studiopage",    
            "studio/"              : "studiopage",    

            "publications"         : "pubspage",
            "publications/"        : "pubspage",
            "/*"                   :  "defaultRoute"
        },
    
    });

    var initialize = function()
    {
        var router = new AppRouter();

        router.on('route:news', function (actions)
        {
            Events.trigger("changeView" ,{"action":"news"});
        });

        router.on('route:defaultRoute', function (actions)
        {
            Events.trigger("changeView" ,{"action":"news"});
        });

        router.on('route:projectspage', function (actions)
        {
            Events.trigger("changeView" ,{"action":"projects"});
        });

        router.on('route:singleproject', function (projectid)
        {
            Events.trigger("changeView" ,{"action":"singleproject", "projectid":projectid});
        });

        router.on('route:pubspage', function ()
        {
            Events.trigger("changeView" ,{"action":"publications"});
        });

        router.on('route:studiopage', function (projectid)
        {
            Events.trigger("changeView" ,{"action":"studio"});
        });

        //Backbone.history.start();
        Backbone.history.start({pushState: true})
    };

    return { initialize: initialize };

});
